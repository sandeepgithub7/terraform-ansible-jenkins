terraform {
  backend "s3" {
    bucket = "soma-awsbucket"
    key    = "Gitlab/terraform.tfstate"
    region = "us-east-1"
  }
}
